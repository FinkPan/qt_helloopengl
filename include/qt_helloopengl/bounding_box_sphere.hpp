#ifndef _HS_GRAPHICS_GRAPHICS_UTILITY_BOUNDING_BOX_SPHERE_HPP_
#define _HS_GRAPHICS_GRAPHICS_UTILITY_BOUNDING_BOX_SPHERE_HPP_

#include <cmath>
#include <QVector3D>

class BoundingBoxSphere
{
public:
  typedef float Scalar;
  typedef QVector3D Vector3;
  BoundingBoxSphere()
  {
    min_[0] = std::numeric_limits<Scalar>::max();
    min_[1] = std::numeric_limits<Scalar>::max();
    min_[2] = std::numeric_limits<Scalar>::max();
    max_[0] = -std::numeric_limits<Scalar>::max();
    max_[1] = -std::numeric_limits<Scalar>::max();
    max_[2] = -std::numeric_limits<Scalar>::max();
  }
  ~BoundingBoxSphere(){}
  void Set(const Vector3 &min,const Vector3 &max)
  {
    min_ = min; 
    max_ = max;
    CalculateSphere();
  }

  void Translate(const Vector3& t)
  {
    min_ += t;
    max_ += t;
    CalculateSphere();
  }

  void Get(Vector3 &min, Vector3 &max) const
  {
    min = min_; 
    max = max_;
  }

  Vector3 GetMin() const { return min_;}
  Vector3 GetMax() const { return max_; }

  Scalar GetMinX() const { return min_[0]; }
  Scalar GetMinY() const { return min_[1]; }
  Scalar GetMinZ() const { return min_[2]; }

  Scalar GetMaxX() const { return max_[0]; }
  Scalar GetMaxY() const { return max_[1]; }
  Scalar GetMaxZ() const { return max_[2]; }

  Vector3 GetCenter() const { return center_;}
  Scalar  GetRadius() const { return radius_;}
  Scalar  GetWidth()  const { return width_; }
  Scalar  GetHeight() const { return height_;}
  Scalar  GetDepth()  const { return depth_; }

  void Scale(Scalar s)
  {
    min_ = min_*s;
    max_ = max_*s;
    CalculateSphere();
  }

  void Add(const BoundingBoxSphere& bb)
  {
     min_[0] = std::min(min_[0], bb.GetMinX());
     min_[1] = std::min(min_[1], bb.GetMinY());
     min_[2] = std::min(min_[2], bb.GetMinZ());

     max_[0] = std::max(max_[0], bb.GetMaxX());
     max_[1] = std::max(max_[1], bb.GetMaxY());
     max_[2] = std::max(max_[2], bb.GetMaxZ());

     CalculateSphere();
  }

private:
  inline void CalculateSphere()
  {
    center_ = (min_ + max_)*Scalar(0.5);
    radius_ = (center_ - min_).length();
    width_ = std::abs(max_[0] - min_[0]);
    height_= std::abs(max_[1] - min_[1]);
    depth_ = std::abs(max_[2] - min_[2]);
  }

  Vector3 center_;
  Scalar radius_;
  Vector3 min_;
  Vector3 max_;
  Scalar width_;
  Scalar height_;
  Scalar depth_;
};

#endif