#ifndef _QT_HELLOOPENGL_TRACKBALL_HPP_
#define _QT_HELLOOPENGL_TRACKBALL_HPP_

#include <cmath>
#include <QVector3D>
#include <QPoint>

class TrackBall
{
public:

  TrackBall(){}
  ~TrackBall(){}

  void SetWindowSize(const int window_width,
                     const int window_height)
  {
    window_width_ = window_width;
    window_height_= window_height;
  }

  //返回 旋转轴 和 旋转角度
  void RotateTrackball(const QPoint start_point, const QPoint end_point,
                       QVector3D &axis, float &angel)
  {

    QPointF point_1 = QPointF((2.0f*start_point.x()-window_width_)/window_width_,
                              (window_height_ - 2.0f*start_point.y())/window_height_);
    QPointF point_2 = QPointF((2.0f*end_point.x()-window_width_)/window_width_,
                               (window_height_ - 2.0f*end_point.y())/window_height_);

    QVector3D p1 = QVector3D(point_1.x(), point_1.y(), tb_project_to_sphere(1,point_1.x(),point_1.y()));
    QVector3D p2 = QVector3D(point_2.x(), point_2.y(), tb_project_to_sphere(1,point_2.x(),point_2.y()));
    
   axis = QVector3D::crossProduct(p1,p2);
   axis.normalize();

    //Figure out how much to rotate around that axis.
    float t = (p2 - p1).length() / (2.0f);

     // Avoid problems with out-of-control values...
     if (t >  1.0) t =  1.0f;
     if (t < -1.0) t = -1.0f;
     angel = std::asin(t) * 180.0f / M_PI *2.0f;
  }

private:
  static float tb_project_to_sphere(float r, float x, float y)
  {
    float n = sqrt( x * x + y * y );
    if(n < r * 0.70710678118654752440f)
      return sqrt( r * r - n * n);
    else
    {
      // On hyperbola
      float t = r / 1.41421356237309504880f;
      return t * t / n;
    }
  }

private:
  int window_width_;
  int window_height_;
};

#endif