###############################################################################
#
set(TREEVIEW_NAME "qtreeview")
set(TREEVIEW_SOURCES
    "main.cpp"
    "TreeView.hpp"
    "TreeView.cpp"
    )
set(TREEVIEW_RESOURCES_FILES 
) 

set(TREEVIEW_QT_MODULES
"Widgets"
"Core"
"Gui"
)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

hs_add_qt_executable(${TREEVIEW_NAME}
  SOURCES ${TREEVIEW_SOURCES}
  QT_MODULES ${TREEVIEW_QT_MODULES}
  RESOURCES ${TREEVIEW_RESOURCES_FILES}
  )
  

