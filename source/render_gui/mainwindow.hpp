#ifndef _MAINWINDOW_HPP_
#define _MAINWINDOW_HPP_

#include <QMainWindow>
#include <QAction>
#include <QMenuBar>
#include <QToolBar>
#include <QMenu>
#include <QStatusBar>
#include <QPushButton>

#include "openglwindow.hpp"

class MainWindow : public QMainWindow
{
  Q_OBJECT
public:
  MainWindow();
  ~MainWindow();

  private slots:
    void NewEmpytProject();
    void Open();
    bool Save();
    void About();
    void RenderCube();
    void RenderTriangle();


private:
  OpenGLWindow* opengl_window_;

  void CreateActions();
  void CreateMenus();
  void CreateToolBars();
  void CreateStatusBar();

  QMenu *FileMenu;
  QMenu *ToolMenu;
  QMenu *RenderMenu;
  QMenu *HelpMenu;
  QToolBar *FileToolBar;
  QToolBar *ToolToolBar;
  QAction *NewAct;
  QAction *OpenAct;
  QAction *SaveAct;
  QAction *ExitAct;
  QAction *RenderCubeAct;
  QAction *RenderTriangleAct;
  QAction *AboutAct;

};

#endif

