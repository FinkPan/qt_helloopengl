#include "Cube.hpp"

#include <QOpenGLShader>


Cube::Cube()
{
  vbo_vertex_data_ = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
  vbo_color_data_     = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
}

Cube::~Cube(){}

GLuint Cube::loadShader(GLenum type, const char *source)
{
  GLuint shader = glCreateShader(type);
  glShaderSource(shader, 1, &source, 0);
  glCompileShader(shader);
  return shader;
}

void Cube::SetupBufferData()
{
  const GLfloat minx = bbs_.GetMinX();
  const GLfloat miny = bbs_.GetMinY();
  const GLfloat minz = bbs_.GetMinZ();

  const GLfloat maxx = bbs_.GetMaxX();
  const GLfloat maxy = bbs_.GetMaxY();
  const GLfloat maxz = bbs_.GetMaxZ();

#define LBN minx,miny,maxz
#define LBF minx,miny,minz
#define LTN minx,maxy,maxz
#define LTF minx,maxy,minz

#define RBN maxx,miny,maxz
#define RBF maxx,miny,minz
#define RTN maxx,maxy,maxz
#define RTF maxx,maxy,minz

  static GLfloat vertex_data[] = 
  {
    LBN,RBN,LTN,
    RTN,LTN,RBN, //��

    RBN,RBF,RTN,
    RTF,RTN,RBF, //��

    RBF,LBF,RTF,
    LTF,RTF,LBF, //��

    LBF,LBN,LTF,
    LTN,LTF,LBN, //��ɫ

    LTN,RTN,LTF,
    RTF,LTF,RTN, //��ɫ

    LBF,RBF,LBN,
    RBN,LBN,RBF,
  };

  static GLfloat color_data[] = 
  {
    1.0f, 0.0f, 0.0f, //��
    1.0f, 0.0f, 0.0f, //��
    1.0f, 0.0f, 0.0f, //��
    1.0f, 0.0f, 0.0f, //��
    1.0f, 0.0f, 0.0f, //��
    1.0f, 0.0f, 0.0f, //��
    0.0f, 1.0f, 0.0f, //��
    0.0f, 1.0f, 0.0f, //��
    0.0f, 1.0f, 0.0f, //��
    0.0f, 1.0f, 0.0f, //��
    0.0f, 1.0f, 0.0f, //��
    0.0f, 1.0f, 0.0f, //��
    0.0f, 0.0f, 1.0f, //��
    0.0f, 0.0f, 1.0f, //��
    0.0f, 0.0f, 1.0f, //��
    0.0f, 0.0f, 1.0f, //��
    0.0f, 0.0f, 1.0f, //��
    0.0f, 0.0f, 1.0f, //��
    0.0f, 1.0f, 1.0f, //��ɫ
    0.0f, 1.0f, 1.0f, //��ɫ
    0.0f, 1.0f, 1.0f, //��ɫ
    0.0f, 1.0f, 1.0f, //��ɫ
    0.0f, 1.0f, 1.0f, //��ɫ
    0.0f, 1.0f, 1.0f, //��ɫ
    1.0f, 1.0f, 0.0f, //��ɫ
    1.0f, 1.0f, 0.0f, //��ɫ
    1.0f, 1.0f, 0.0f, //��ɫ
    1.0f, 1.0f, 0.0f, //��ɫ
    1.0f, 1.0f, 0.0f, //��ɫ
    1.0f, 1.0f, 0.0f, //��ɫ
    1.0f, 0.0f, 1.0f, //��ɫ
    1.0f, 0.0f, 1.0f, //��ɫ
    1.0f, 0.0f, 1.0f, //��ɫ
    1.0f, 0.0f, 1.0f, //��ɫ
    1.0f, 0.0f, 1.0f, //��ɫ
    1.0f, 0.0f, 1.0f  //��ɫ
  };

  glGenBuffers(1, &positionAttr);
  glBindBuffer(GL_ARRAY_BUFFER, positionAttr);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data) ,vertex_data, GL_STATIC_DRAW);

  glGenBuffers(1, &colorAttr);
  glBindBuffer(GL_ARRAY_BUFFER, colorAttr);
  glBufferData(GL_ARRAY_BUFFER, sizeof(color_data) ,color_data, GL_STATIC_DRAW);


  glGenVertexArrays(1,&vao_);
  glBindVertexArray(vao_);
  glBindBuffer(GL_ARRAY_BUFFER, positionAttr);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glBindBuffer(GL_ARRAY_BUFFER, colorAttr);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glBindVertexArray(0);

}

void Cube::initialize()
{
  QOpenGLShader *vshader = new QOpenGLShader(QOpenGLShader::Vertex, this);
  const char *vsrc =
    "#version 330 core\n"
    "layout(location = 0) in vec3 setPosition;\n"
    "layout(location = 1) in vec3 setColor;\n"
    "out vec3 color;\n"
    "uniform  mat4 MVP;\n"
    "void main()\n"
    "{\n"
    "gl_Position = MVP * vec4(setPosition, 1.0);\n"
    "color = setColor;\n"
    "}\n";
  vshader->compileSourceCode(vsrc);

  QOpenGLShader *fshader = new QOpenGLShader(QOpenGLShader::Fragment, this);
  const char *fsrc =
    "#version 330 core\n"
    "in vec3 color;\n"
    "out vec3 Outcolor;\n"
    "void main()\n"
    "{\n"
    "Outcolor = color;\n"
    "}\n" ;
  fshader->compileSourceCode(fsrc);

  programID_ = new QOpenGLShaderProgram(this);
  programID_->addShader(vshader);
  programID_->addShader(fshader);
  programID_->link();
  positionAttr = programID_->attributeLocation("setPosition");
  colorAttr    = programID_->attributeLocation("setColor");
  matrixAttr   = programID_->uniformLocation("MVP");

  Float minx =  1.0f; Float miny =   -2.0f; Float minz =  3.0f;
  Float maxx =  3.0f; Float maxy =   0.0f; Float maxz =   5.0f;
  bbs_.Set(QVector3D(minx,miny,minz),QVector3D(maxx,maxy,maxz));
  initmvp();

  SetupBufferData();

  glClearColor(0.2f,0.2f,0.2f,1.0f);
}

void Cube::render()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  programID_->bind();

  mvp_ = project_matrix_ * view_matrix_ * model_matrix_;
  glUniformMatrix4fv(matrixAttr,1,GL_FALSE,mvp_.data());

  glBindVertexArray(vao_);
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glDrawArrays(GL_TRIANGLES,0,36);
  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glBindVertexArray(0);
  glFlush();

  programID_->release();

}
