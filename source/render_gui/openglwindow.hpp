#ifndef _OPENGLWINDOW_HPP
#define _OPENGLWINDOW_HPP

#include <iostream>

#if defined(WIN32)
#include <GL/glew.h>
#endif

#include <QtGui/QWindow>
#include <QMouseEvent>
#include <QMatrix4x4>
#include <QVector3D>
#include <QVector4D>
#include <QDataStream>
#include <QDebug>
#include <QtCore>

#include "qt_helloopengl/trackball.hpp"
#include "qt_helloopengl/bounding_box_sphere.hpp"

QT_BEGIN_NAMESPACE
class QPainter;
class QOpenGLContext;
class QOpenGLPaintDevice;
QT_END_NAMESPACE

class OpenGLWindow : public QWindow
{
  Q_OBJECT
public:
  typedef float Float;

  explicit OpenGLWindow(QWindow *parent = 0);
  ~OpenGLWindow();

  virtual void render(QPainter *painter);
  virtual void render();

  virtual void initialize();

  void setAnimating(bool animating);

protected:
  bool event(QEvent *event);
  void exposeEvent(QExposeEvent *event);
  void mousePressEvent(QMouseEvent* event);
  void mouseReleaseEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void wheelEvent(QWheelEvent *event);
  void resizeEvent(QResizeEvent* event);
  void keyPressEvent(QKeyEvent *event);

  void initmvp();

  QMatrix4x4 project_matrix_;
  QMatrix4x4 view_matrix_;
  QMatrix4x4 model_matrix_;
  QMatrix4x4 mvp_;

  TrackBall tb_;
  BoundingBoxSphere bbs_;

private:
  void renderLater();
  void renderNow();

  float setFrustum();
  bool getFrustum( Float& left,   Float& right,
    Float& bottom, Float& top,
    Float& zNear,  Float& zFar ) const;


  void clampProjectionMatrix();

  void Scale(Float s);
  void Translate(QVector3D t);
  void Rotate(Float angel, QVector3D axis);

  bool m_update_pending;
  bool m_animating;

  QOpenGLContext *m_context;
  QOpenGLPaintDevice *m_device;

  QPoint current_pos_;
  QPoint previous_pos_;

};

#endif


