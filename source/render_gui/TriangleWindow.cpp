#include "TriangleWindow.hpp"

TriangleWindow::TriangleWindow()
  : m_program(0)
  , m_frame(0)
{
}

static const char *vertexShaderSource =
  "attribute highp vec4 posAttr;\n"
  "attribute lowp vec4 colAttr;\n"
  "varying lowp vec4 col;\n"
  "uniform highp mat4 matrix;\n"
  "void main() {\n"
  "   col = colAttr;\n"
  "   gl_Position = matrix * posAttr;\n"
  "}\n";

static const char *fragmentShaderSource =
  "varying lowp vec4 col;\n"
  "void main() {\n"
  "   gl_FragColor = col;\n"
  "}\n";

GLuint TriangleWindow::loadShader(GLenum type, const char *source)
{
  GLuint shader = glCreateShader(type);
  glShaderSource(shader, 1, &source, 0);
  glCompileShader(shader);
  return shader;
}

void TriangleWindow::initialize()
{
  m_program = new QOpenGLShaderProgram(this);
  m_program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
  m_program->addShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
  m_program->link();
  m_posAttr = m_program->attributeLocation("posAttr");
  m_colAttr = m_program->attributeLocation("colAttr");
  m_matrixUniform = m_program->uniformLocation("matrix");
  glClear(GL_COLOR_BUFFER_BIT);
}

void TriangleWindow::render()
{
  const qreal retinaScale = devicePixelRatio();
  glViewport(0, 0, width() * retinaScale, height() * retinaScale);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  m_program->bind();

  QMatrix4x4 matrix;
  matrix.setToIdentity();
  matrix.perspective(60, 4.0f/3.0f, 0.1f, 100.0f);
  matrix.translate(0, 0, -2);
  matrix.rotate(100.0f * m_frame / screen()->refreshRate(), 0, 1, 0);


  m_program->setUniformValue(m_matrixUniform, matrix);

  GLfloat vertices[] = {
    0.0f,  0.5f,0.0f,
    -0.5f,  0.0f,0.0f,
    0.5f,  0.0f,0.0f,

    0.3f,  0.5f,-1.0f,
    -0.3f,  0.0f,-1.0f,
    0.8f,  0.0f, 1.0f
  };

  GLfloat colors[] = {
    1.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, 0.0f
  };

  glVertexAttribPointer(m_posAttr, 3, GL_FLOAT, GL_FALSE, 0, vertices);
  glVertexAttribPointer(m_colAttr, 3, GL_FLOAT, GL_FALSE, 0, colors);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  glDrawArrays(GL_TRIANGLES, 0, 6);

  glDisableVertexAttribArray(1);
  glDisableVertexAttribArray(0);

  m_program->release();

  ++m_frame;
}
//! [5]

