#ifndef _TRIANGLEWINDOW_HPP_
#define _TRIANGLEWINDOW_HPP_

#include "openglwindow.hpp"
#include "qt_helloopengl/trackball.hpp"

#include <QtGui/QGuiApplication>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QScreen> 
#include <QVector3D>


class TriangleWindow : public OpenGLWindow
{
public:
  TriangleWindow();
  TriangleWindow(QVector3D v1,QVector3D v2, QVector3D v3)
    :v1_(v1),v2_(v2),v3_(v3)
  {}

  void initialize();
  void render();

private:
  GLuint loadShader(GLenum type, const char *source);

  GLuint m_posAttr;
  GLuint m_colAttr;
  GLuint m_matrixUniform;

  QOpenGLShaderProgram *m_program;
  int m_frame;
  QVector3D v1_;
  QVector3D v2_;
  QVector3D v3_;

};

#endif