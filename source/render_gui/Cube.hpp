#ifndef _CUBE_HPP_
#define _CUBE_HPP_

#include "openglwindow.hpp"


#include <QtGui/QGuiApplication>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QScreen> 
#include <QVector3D>
#include <QOpenGLBuffer>

class Cube : public OpenGLWindow
{
public:
  Cube();
  ~Cube();

  void initialize();
  void render();

private:
  void SetupBufferData();
private:
  GLuint loadShader(GLenum type, const char *source);

  QOpenGLShaderProgram *programID_;

  QOpenGLBuffer vbo_vertex_data_;
  QOpenGLBuffer vbo_color_data_;

  GLuint vao_;
  GLuint positionAttr;
  GLuint colorAttr;
  GLuint matrixAttr;

};

#endif