#include "openglwindow.hpp"

#include <iostream>
#include <QtCore/QCoreApplication>

#include <QtGui/QOpenGLContext>
#include <QtGui/QOpenGLPaintDevice>
#include <QtGui/QPainter>

OpenGLWindow::OpenGLWindow(QWindow *parent)
  : QWindow(parent)
  , m_update_pending(false)
  , m_animating(false)
  , m_context(0)
  , m_device(0)
{
  setSurfaceType(QWindow::OpenGLSurface);
  QSurfaceFormat format;
  format.setDepthBufferSize(24);
  format.setSamples(16);
  setFormat(format);

  resize(500,500);
  tb_.SetWindowSize(500,500);
}

OpenGLWindow::~OpenGLWindow()
{
  delete m_device;
}
void OpenGLWindow::render(QPainter *painter)
{
  Q_UNUSED(painter);
}

void OpenGLWindow::initialize()
{
}

void OpenGLWindow::render()
{
  if (!m_device)
    m_device = new QOpenGLPaintDevice;

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  m_device->setSize(size());

  QPainter painter(m_device);
  render(&painter);
}

void OpenGLWindow::renderLater()
{
  if (!m_update_pending) {
    m_update_pending = true;
    QCoreApplication::postEvent(this, new QEvent(QEvent::UpdateRequest));
  }
}

bool OpenGLWindow::event(QEvent *event)
{
  switch (event->type()) {
  case QEvent::UpdateRequest:
    m_update_pending = false;
    renderNow();
    return true;
  default:
    return QWindow::event(event);
  }
  return true;
}

void OpenGLWindow::exposeEvent(QExposeEvent *event)
{
  Q_UNUSED(event);

  if (isExposed())
    renderNow();
}

void OpenGLWindow::mousePressEvent(QMouseEvent* event)
{
  current_pos_ = event->pos();
  previous_pos_ = event->pos();
}

void OpenGLWindow::mouseReleaseEvent(QMouseEvent* event)
{
  previous_pos_ = event->pos();
}

void OpenGLWindow::mouseMoveEvent(QMouseEvent *event)
{

  if (event->buttons() & Qt::LeftButton)
  {
    current_pos_ = event->pos();                                              
    QVector3D axis;
    Float angel = 0;
    tb_.RotateTrackball(previous_pos_,current_pos_,axis,angel);

    Rotate(angel,axis);

    previous_pos_ = current_pos_;
    renderNow();
  }
  else if (event->buttons() & Qt::RightButton)
  {
  }
  else if(event->buttons() & Qt::MidButton)
  {
    current_pos_ = event->pos();

    Float left  ;
    Float right ;
    Float bottom;
    Float top   ;
    Float z_near;
    Float z_far ;

    getFrustum(left,right,bottom,top,z_near,z_far);
    Float  radius = bbs_.GetRadius();

    // try to compute dist from frustrum
    Float vertical2   = std::fabs(right - left) / z_near / 2.;
    Float horizontal2 = std::fabs(top - bottom) / z_near / 2.;
    Float dim         = std::min(horizontal2, vertical2);
    Float viewAngle   = std::atan2(dim,1.);
    Float dist = radius / std::sin(viewAngle);

    Float offset_x = Float((current_pos_ - previous_pos_).x()) / Float(width() ) *
      std::abs(right-left) * dist / z_near;
    Float offset_y = Float((current_pos_ - previous_pos_).y()) / Float(height()) *
      std::abs(top-bottom) * dist / z_near;

    Translate(QVector3D(offset_x, -offset_y, 0));
    previous_pos_ = current_pos_;
    renderNow();
  }
}

void OpenGLWindow::wheelEvent(QWheelEvent *event)
{
  const Float base = Float(1.001);
  Float scale = std::pow(base, Float(-event->angleDelta().y()));
  Scale(scale);
  renderNow();
}

void OpenGLWindow::initmvp()
{
  Float dist = setFrustum();

  QVector3D center = bbs_.GetCenter();
  view_matrix_.setToIdentity();
  view_matrix_.lookAt(
    QVector3D(center[0],center[1], center[2]+dist),
    QVector3D(center[0],center[1], center[2]),
    QVector3D(0.0f,1.0f, 0.0f));

  model_matrix_.setToIdentity();
}

float OpenGLWindow::setFrustum()
{
  Float  radius = bbs_.GetRadius();

  Float left   = -radius*0.1;
  Float right = radius*0.1;
  Float bottom = -radius*0.1;
  Float top   = radius*0.1;
  Float z_near   =  1.0f;

  int vw = width();
  int vh = height();

  Float w = right - left;
  Float h = top - bottom;
  Float width_scale = w / Float(vw);
  Float height_scale= h / Float(vh);

  Float scale = std::max(width_scale, height_scale);

  Float ratio = Float(h)/Float(w); 

  if (width_scale > height_scale)
  {
    bottom -= (vh * scale - h) * Float(0.5);
    top    += (vh * scale - h) * Float(0.5);
  }
  else
  {
    left  -= (vw * scale - w) * Float(0.5);
    right += (vw * scale - w) * Float(0.5);
  }

  // try to compute dist from frustrum
  Float vertical2   = std::fabs(right - left) / z_near / 2.;
  Float horizontal2 = std::fabs(top - bottom) / z_near / 2.;
  Float dim         = std::min(horizontal2, vertical2);
  Float viewAngle   = std::atan2(dim,1.);
  Float dist = radius / std::sin(viewAngle);
  Float z_far   = 10000.0f > dist + radius ? 10000.0f : dist + radius;

  project_matrix_.setToIdentity();
  project_matrix_.frustum(left,right,bottom,top,z_near,z_far);

  return dist;
}

bool OpenGLWindow::getFrustum( Float& left,   Float& right,
                              Float& bottom, Float& top,
                              Float& zNear,  Float& zFar ) const
{
  if (project_matrix_(3,0)!=0.0f || project_matrix_(3,1)!=0.0f || project_matrix_(3,2)!=-1.0f || project_matrix_(3,3)!=0.0f) return false;

  zNear = project_matrix_(2,3) / (project_matrix_(2,2)-1.0);
  zFar = project_matrix_(2,3) / (1.0+project_matrix_(2,2));

  left = zNear * (project_matrix_(0,2)-1.0) / project_matrix_(0,0);
  right = zNear * (1.0+project_matrix_(0,2)) / project_matrix_(0,0);

  top = zNear * (1.0+project_matrix_(1,2)) / project_matrix_(1,1);
  bottom = zNear * (project_matrix_(1,2)-1.0) / project_matrix_(1,1);

  return true;
}

//TODO not complete
void OpenGLWindow::clampProjectionMatrix()
{
  //   Float  radius = bbs_.GetRadius();
  //   Float nearFarRatio = 0.0005f;
  // 
  //   Float znear/* = dist_-radius*/;
  //   Float zfar/*  = dist_+radius*/;
  // 
  //   Float zfarPushRatio = 1.02f;
  //   Float znearPullRatio = 0.98f;
  // 
  //   //znearPullRatio = 0.99;
  // 
  //   Float desired_znear = znear * znearPullRatio;
  //   Float desired_zfar = zfar * zfarPushRatio;
  // 
  //   //near plane clamping.
  //   double min_near_plane = zfar*nearFarRatio;
  //   if (desired_znear<min_near_plane) desired_znear=min_near_plane;
  // 
  //   //assign the clamped values back to the computed values.
  //   znear = desired_znear;
  //   zfar = desired_zfar;
  // //   std::cout << "znear: " << znear << "\n";
  // //   std::cout << "zfar: "  << zfar << "\n";
  // 
  //   QMatrix4x4 projection = project_matrix_;
  //   Float trans_near_plane = (-desired_znear*projection(2,2)+projection(3,2))/(-desired_znear*projection(2,3)+projection(3,3));
  //   Float trans_far_plane  = (-desired_zfar *projection(2,2)+projection(3,2))/(-desired_zfar *projection(2,3)+projection(3,3));
  // //   std::cout << "trans_near_plane: " << trans_near_plane << "\n";
  // //   std::cout << "trans_far_plane: "  << trans_far_plane  << "\n";
  // 
  //   Float ratio = fabs(2.0/(trans_near_plane-trans_far_plane));
  //   Float center = -(trans_near_plane+trans_far_plane)/2.0;
  // 
  //   QMatrix4x4 posemult = QMatrix4x4(1.0f,0.0f,0.0f,0.0f,
  //                                  0.0f,1.0f,0.0f,0.0f,
  //                                  0.0f,0.0f,ratio,0.0f,
  //                                  0.0f,0.0f,center*ratio,1.0f);
  // //   project_matrix_ = posemult * project_matrix_;

}

void OpenGLWindow::Scale(Float s)
{
  //缩放矩阵
  QMatrix4x4 SM;
  SM.setToIdentity();
  SM.scale(s, s, s);

  //将物体中心移动到世界坐标原点再缩放.
  QMatrix4x4 Tc;
  Tc.setToIdentity();
  QVector3D center = bbs_.GetCenter();
  Tc.translate(-center[0],-center[1],-center[2]);
  model_matrix_ = Tc.inverted() * SM * Tc * model_matrix_;
}

void OpenGLWindow::Translate(QVector3D t)
{
  QMatrix4x4 TM;
  TM.setToIdentity();
  TM.translate(t);
  model_matrix_ = TM * model_matrix_;
}

void OpenGLWindow::Rotate(Float angel, QVector3D axis)
{
  QMatrix4x4 RM;
  RM.setToIdentity();
  RM.rotate(angel,axis);

  //将物体中心移动到世界坐标原点再旋转.
  QMatrix4x4 Tc;
  Tc.setToIdentity();
  QVector3D center = bbs_.GetCenter();
  Tc.translate(-center[0],-center[1],-center[2]);
  model_matrix_ = Tc.inverted() * RM * Tc * model_matrix_;
}

void OpenGLWindow::resizeEvent(QResizeEvent* event)
{
  int w = width();
  int h = height();
  glViewport(0, 0, w, h);
  setFrustum();
  tb_.SetWindowSize(w,h);
}

void OpenGLWindow::keyPressEvent(QKeyEvent *event)
{
  switch (event->key())
  {
  case Qt::Key_Space:
    model_matrix_.setToIdentity();
    break;
  default:
    break;
  }
  renderNow();

}

void OpenGLWindow::renderNow()
{
  if (!isExposed())
    return;

  bool needsInitialize = false;

  if (!m_context) {
    m_context = new QOpenGLContext(this);
    m_context->setFormat(requestedFormat());
    m_context->create();

    needsInitialize = true;
  }

  m_context->makeCurrent(this);

  if (needsInitialize) {
#if defined WIN32
    glewExperimental = GL_TRUE;
    int result = glewInit();
    if (result != GLEW_OK)
    {
      std::cout << "glewInit failure.";
      return;
    }
#endif
    initialize();
    glViewport(0, 0, width(), height());

  }


  render();

  m_context->swapBuffers(this);

  if (m_animating)
    renderLater();
}

void OpenGLWindow::setAnimating(bool animating)
{
  m_animating = animating;

  if (animating)
    renderLater();
}
