#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <vector>
#include <map>
#include <string>

#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QWidget>
#include <QLineEdit>
#include <QFileDialog>
#include <QString>

class MyWidget :public QWidget
{
  Q_OBJECT
public:
  MyWidget(QWidget *parent = 0);
  ~MyWidget();

  QString group_name();

public slots:
  void OnClicked();

private:
  QHBoxLayout* layout_;
  QString group_name_;
  QLineEdit* line_edit_;
  QString group_dir_;
  QPushButton* button_;
};


class MyMainWidget : public QWidget
{
  //Q_OBJECT
public:
  MyMainWidget(std::map<int, std::string>& remodify, 
                bool is_directory = true,
                QWidget *parent = 0);
  ~MyMainWidget();

public:

private:
  QLabel group_name_;
  std::vector<MyWidget*> my_widgets_;
  std::map<int,std::string> group_paths_;
  QVBoxLayout* main_v_layout_;

};



#endif