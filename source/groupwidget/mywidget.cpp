#include "mywidget.hpp"

MyWidget::MyWidget(QWidget *parent)
  :QWidget(parent)
{
  layout_ = new QHBoxLayout(this);
  line_edit_ = new QLineEdit(this);
  button_ = new QPushButton(tr("Browse..."), this);
  layout_->addWidget(line_edit_);
  layout_->addWidget(button_);
  setLayout(layout_);
  connect(button_, &QPushButton::clicked, this, &MyWidget::OnClicked);
}

MyWidget::~MyWidget()
{
}
QString MyWidget::group_name()
{
  return group_name_;
}

void MyWidget::OnClicked()
{
  group_dir_ = QFileDialog::getExistingDirectory(
    this, 
    tr("Open Directory"),
    ".", 
    QFileDialog::ShowDirsOnly| QFileDialog::DontResolveSymlinks);
  line_edit_->setText(group_dir_);
}


MyMainWidget::MyMainWidget(std::map<int, std::string>& remodify,
                           bool is_directory = true,
                           QWidget *parent)
  :QWidget(parent)
{
  main_v_layout_ = new QVBoxLayout;
  setLayout(main_v_layout_);

  QSpacerItem* spacerItem = new QSpacerItem(
    0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

  QLabel* label_main = 
    new QLabel(tr("The path of the following photo group needs to be changed!"));
  main_v_layout_->addWidget(label_main);

  for (int i = 0; i < group_names.size(); ++i)
  {
    QLabel* label_group_name = new QLabel(group_names.at(i)+":");
    label_group_name->setMinimumWidth(50);
    QHBoxLayout* layout_item = new QHBoxLayout;
    layout_item->addWidget(label_group_name);
    MyWidget* my_widget = new MyWidget(this);
    my_widgets_.push_back(my_widget);
    layout_item->addWidget(my_widget);
    main_v_layout_->addLayout(layout_item);
  }
  main_v_layout_->addItem(spacerItem);
  QHBoxLayout* control_v_layout = new QHBoxLayout;
  QPushButton* button_ok = new QPushButton(tr("OK"), this);
  QPushButton* button_cancel = new QPushButton(tr("Cancel"), this);
  control_v_layout->addWidget(button_ok);
  control_v_layout->addWidget(button_cancel);
  control_v_layout->setAlignment(Qt::AlignRight);
  main_v_layout_->addLayout(control_v_layout);


}
MyMainWidget::~MyMainWidget()
{

}
