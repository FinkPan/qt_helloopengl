// #include <QtGui>
#include <QApplication>
#include <QSurfaceFormat>

// #include <QGraphicsScene>
// #include <QGraphicsView>
// #include <QGraphicsItem>
// #include <QOpenGLWidget>
// 
// #include "qgraphics.hpp"
#include "mainwindow.hpp"


int main(int argc, char** argv)
{
//   QApplication app(argc,argv);
// 
//   QGraphicsScene scene;
//   LayerWidgetItem *lwi = new LayerWidgetItem;
//   QGraphicsTextItem*item = new QGraphicsTextItem("Hello, world!");
//   QGraphicsTextItem*item1 = new QGraphicsTextItem("Hell22222o, world!");
//   scene.addItem(lwi);
//   scene.addItem(item);
//   scene.addItem(item1);
//   QGraphicsView view(&scene);
//   view.resize(800,600);
//   view.show();
// 
//   return app.exec();
  QApplication app(argc, argv);
  QSurfaceFormat format;
  if (QCoreApplication::arguments().contains(QLatin1String("--multisample")))
    format.setSamples(4);
  if (QCoreApplication::arguments().contains(QLatin1String("--coreprofile"))) {
    format.setVersion(3, 3);
    format.setProfile(QSurfaceFormat::CoreProfile);
  }
  format.setDepthBufferSize(24);
  format.setStencilBufferSize(8);
  QSurfaceFormat::setDefaultFormat(format);

  MainWindow mw;
  mw.showMaximized();

  return app.exec();

}