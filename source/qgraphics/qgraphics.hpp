#ifndef QT_HELLOOPENGL_QGRAPHICS_QGRAPHICS_HPP_
#define QT_HELLOOPENGL_QGRAPHICS_QGRAPHICS_HPP_

#include <QGraphicsItem>
#include <QPainter>

#include "qopenglwidget.hpp"

class LayerWidgetItem : public GLWidget, public QGraphicsItem
{
public:
  LayerWidgetItem(QWidget *parent);
  ~LayerWidgetItem();

   QRectF boundingRect() const;
   void paint(QPainter *,const QStyleOptionGraphicsItem *,QWidget *);

private:
   //GLWidget *glwidget_;
};


#endif // !QT_HELLOOPENGL_QGRAPHICS_QGRAPHICS_HPP_
