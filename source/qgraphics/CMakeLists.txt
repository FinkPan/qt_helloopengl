###############################################################################
#qgraphics
set(QGRAPHICS_NAME "qgraphics")
set(QGRAPHICS_SOURCES
    "main.cpp"
    "qgraphics.hpp"
    "qgraphics.cpp"
    "mainwindow.hpp"
    "mainwindow.cpp"
    "qopenglwidget.hpp"
    "qopenglwidget.cpp"
    )
set(QGRAPHICS_RESOURCES_FILES 
  "../../resource/qgraphics.qrc"
) 

set(QGRAPHICS_QT_MODULES
"Widgets"
"Core"
"OpenGL"
"Gui"
)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

hs_add_qt_executable(${QGRAPHICS_NAME}
  SOURCES ${QGRAPHICS_SOURCES}
  QT_MODULES ${QGRAPHICS_QT_MODULES}
  RESOURCES ${QGRAPHICS_RESOURCES_FILES}
  )
  
if(OPENGL_FOUND)
  target_link_libraries(${QGRAPHICS_NAME} ${OPENGL_LIBRARIES})
endif()
hslib_post_build(${QGRAPHICS_NAME})

