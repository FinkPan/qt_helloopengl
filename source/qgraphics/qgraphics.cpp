#include "qgraphics.hpp"

LayerWidgetItem::LayerWidgetItem(QWidget *parent)
  :GLWidget(parent)
{
//   glwidget_ = &glw;
  initializeGL();
}

LayerWidgetItem::~LayerWidgetItem()
{
}

QRectF LayerWidgetItem::boundingRect() const
{
//     qreal adjust=0.5;
    return QRectF(-0.0,0.0,4.0,4.0);
}

void LayerWidgetItem::paint(QPainter *painter,const QStyleOptionGraphicsItem *option,QWidget *widget)
{
    //painter->drawRect(0,0,200,200);
     painter->beginNativePainting();
     paintGL();
     painter->endNativePainting();
}
