#include <QApplication>
#include <QStringList>

#include "myqlistwidget.hpp"

int main(int argc, char **argv)
{
  QApplication app(argc, argv);

  PhotoID_Paths photo_id_path;
  photo_id_path.insert(std::make_pair(1,
    std::string("E:/workspace/data/testmove/img/DSC09751.JPG")));
  photo_id_path.insert(std::make_pair(2,
    std::string("E:/workspace/data/testmove/img/DSC09752.JPG")));
  photo_id_path.insert(std::make_pair(3,
    std::string("E:/workspace/data/testmove/img1/DSC08942_.JPG")));
  photo_id_path.insert(std::make_pair(4,
    std::string("E:/workspace/data/testmove/img1/DSC08943_.JPG")));
  photo_id_path.insert(std::make_pair(5,
    std::string("E:/workspace/data/testmove/img3/DSC09245.JPG")));
  photo_id_path.insert(std::make_pair(6,
    std::string("E:/workspace/data/testmove/img3/DSC09246.JPG")));
  MainWidget* main_widget = new MainWidget(photo_id_path);
  main_widget->show();

  return app.exec();
}
