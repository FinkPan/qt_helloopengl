#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <map>
#include <string>
#include <set>
#include <iostream>

#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QLabel>
#include <QObject>
#include <QPushButton>
#include <QHBoxLayout>
#include <QWidget>
#include <QLineEdit>
#include <QMessageBox>
#include <QFileDialog>
#include <QString>
#include <QTreeView>
#include <QHeaderView>

#include <boost/filesystem.hpp>

typedef int PhotoID;
typedef std::pair<int, std::string> PhotoID_Path;
typedef std::map<int, std::string> PhotoID_Paths;
typedef std::multimap<std::string, PhotoID_Path> PhotoGroupPathIDs;

class ClusterWidget :public QWidget
{
  Q_OBJECT
public:
  ClusterWidget(const QString cluster_name,
    QTreeWidgetItem* item, QWidget *parent = 0);
  ~ClusterWidget();

  QString cluster_dir() const;

public slots:
  void OnClicked();

signals:
  void UpdateClusterDir(const QString cluster_dir, QTreeWidgetItem* item);

private:
  QHBoxLayout* layout_;
  QLabel* label_cluster_name_;
  QLineEdit* line_edit_;
  QString cluster_dir_;
  QPushButton* button_;
  bool is_directory_;
  QTreeWidgetItem* item_;
};

class PhotoItem: public QTreeWidgetItem
{
public:
  PhotoItem(int photo_id, QString photo_path);
  ~PhotoItem();

  int photo_id();
  QString photo_path();

private:
  bool is_exist_;
  int photo_id_;
  QString photo_path_;

};


class MainWidget: public QWidget
{
  Q_OBJECT
public:
  MainWidget(PhotoID_Paths& photo_id_path, QWidget *parent = 0);
  ~MainWidget();

public slots:
  void OnPushButtonIgnoreClicked();
  void OnPushButtonOKClicked();
  void OnPushButtonCancelClicked();

  void UpdateClusterDir(QString cluster_dir, QTreeWidgetItem* item);

signals:
  void PhotoReModify(int error_code);

private:
  //Cluster the photo path
  void ClusterPhotoPath();
  //Add Items
  void AddItems();

private:
  QTreeWidget* tree_widget_;
  QVBoxLayout* main_v_layout_;
  QLabel* label_inform_;
  QPushButton* push_button_ok_;
  QPushButton* push_button_cancel_;
  QPushButton* push_button_ignore_;

  PhotoID_Paths photo_id_path_;
  PhotoGroupPathIDs photo_cluster_id_name_;
  std::vector<QTreeWidgetItem* > top_items_;

};



#endif