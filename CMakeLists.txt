cmake_minimum_required(VERSION 2.8.11)

include("cmake/hslib.cmake")
include("cmake/hsprj.cmake")
include("cmake/hsgui.cmake")

set(HSPRJ_NAME "qt_helloopengl")
hs_project(${HSPRJ_NAME})

find_package(OpenGL)
if(NOT OPENGL_FOUND)
  message("ERROR: OpenGL not found")
endif(NOT OPENGL_FOUND)

include_directories("include")

###############################################################################
#render_gui
option(BUILD_render_gui "build renderer_gui" OFF)
if(BUILD_render_gui)
add_subdirectory(./source/render_gui)
endif()

###############################################################################
#qopenglwidget
option(BUILD_qopenglwidget "build qopenglwidget" OFF)
if(BUILD_qopenglwidget)
add_subdirectory(./source/qopenglwidget)
endif()

###############################################################################
#qgraphics
option(BUILD_qgraphics "build qgraphics" OFF)
if(BUILD_qgraphics)
add_subdirectory(./source/qgraphics)
endif()

###############################################################################
#qlistwidget
option(BUILD_qlistwidget "build qlistwidget" OFF)
if(BUILD_qlistwidget)
add_subdirectory(./source/qlistwidget)
endif()

###############################################################################
#groupwidget
option(BUILD_groupwidget "build groupwidget" OFF)
if(BUILD_groupwidget)
add_subdirectory(./source/groupwidget)
endif()

###############################################################################
#qtreeview
option(BUILD_qtreeview "build qtreeview" OFF)
if(BUILD_qtreeview)
add_subdirectory(./source/qtreeview)
endif()
